package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Modele.Chronologie;
import Modele.Date;
import Modele.Evenement;

public class ChronologieTest {

	@Test
	public void testAjout() {
		
		Chronologie chrono = new Chronologie(new Date(01,01,2010), new Date(01, 01, 2020), 2, "test", "test");
		Evenement evt = new Evenement(new Date(01,01,2011), "test", "test", "test", 2);
		chrono.ajout(evt);
		if(chrono.getTabEvt().containsKey(2011)!=true || chrono.getTabEvt().get(2011).contains(evt)!=true){
			fail("Ajout d'une nouvelle cl�");
		}
		
		Evenement evt2 = new Evenement(new Date(01,01,2011), "test", "test", "test", 3);
		chrono.ajout(evt2);
		
		if(chrono.getTabEvt().containsKey(2011)!=true || chrono.getTabEvt().get(2011).contains(evt2)!=true || chrono.getTabEvt().get(2011).size()!=2){
			fail("Ajout au TreeSet d'une cl� d�j� existante");
		}
		
	}

	@Test
	public void testSupprimerEvenement() {
		Chronologie chrono = new Chronologie(new Date(01,01,2010), new Date(01, 01, 2020), 2, "test", "test");
		Evenement evt = new Evenement(new Date(01,01,2011), "test", "test", "test", 2);
		chrono.ajout(evt);
		
		
		Evenement evt2 = new Evenement(new Date(01,01,2011), "test", "test", "test", 3);
		chrono.ajout(evt2);
		
		chrono.supprimerEvenement(evt);
		if(chrono.getTabEvt().containsKey(2011)!=true || chrono.getTabEvt().get(2011).contains(evt)!=false || chrono.getTabEvt().get(2011).size()!=1){
			fail("Suppression d'un �v�nement dans un TreeSet de 2 �v�nements");
		}
		
		chrono.supprimerEvenement(evt2);
		if(chrono.getTabEvt().get(2011).contains(evt2)!=false || chrono.getTabEvt().get(2011).size()!=0){
			fail("Suppression d'un �v�nement dans un TreeSet de 1 �v�nement");
		}
	}



	@Test
	public void testGetSizeTabEvt() {
		Chronologie chrono = new Chronologie(new Date(01,01,2010), new Date(01, 01, 2020), 2, "test", "test");
		Evenement evt = new Evenement(new Date(01,01,2011), "test", "test", "test", 2);
		chrono.ajout(evt);
		
		
		Evenement evt2 = new Evenement(new Date(01,01,2011), "test", "test", "test", 3);
		chrono.ajout(evt2);
		
		if(chrono.getSizeTabEvt()!=2){
			fail("Not yet implemented");
		}
		
		
	}

	@Test
	public void testEstValide() {
		Chronologie chrono = new Chronologie(new Date(01,01,2010), new Date(01, 01, 2020), 2, "test", "test");
		Chronologie chrono1 = new Chronologie(new Date(01,01,2012), new Date(01, 01, 2010), 2, "test", "test");
		
		if(chrono.estValide()!=true){
			fail("Chronologie de 2010 � 2020");
		}
		
		if(chrono1.estValide()!=false){
			fail("Chronologie de 2012 � 2010");
		}
		
	}

}
