package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Modele.Date;
import Modele.Evenement;

public class EvenementTest {


	@Test
	public void testCompareTo() {
		Evenement evt = new Evenement(new Date(01,01,2012),"test", "test", "test", 2);
		Evenement evt1 = new Evenement(new Date(01,01,2015),"test", "test", "test", 2);
		Evenement evt2 = new Evenement(new Date(01,01,2011),"test", "test", "test", 2);
		Evenement evt3 = new Evenement(new Date(01,01,2012),"test", "test", "test", 3);
		Evenement evt4 = new Evenement(new Date(01,01,2012),"test", "test", "test", 2);
		
		if(evt.compareTo(evt4)!=0){
			fail("M�me �v�nement");
		}
		
		if(evt.compareTo(evt1)>=0){
			fail("�v�nement en param�tre plus tard");
		}
		
		if(evt.compareTo(evt2)<=0){
			fail("�v�nement en param�tre plus t�t");
		}
		
		if(evt.compareTo(evt3)>=0){
			fail("M�me date mais �v�nement en param�tre avec un poids plus fort");
		}
		
	}

	@Test
	public void testEstComprisEntre() {
		
		Evenement evt = new Evenement(new Date(01,01,2012),"test", "test", "test", 2);
		
		if(evt.estComprisEntre(2011, 2013)!=true){
			fail("Entre 2011 et 2013 avec 2012");
		}
		if(evt.estComprisEntre(2012, 2013)!=true){
			fail("Entre 2012 et 2013 avec 2012");
		}
		if(evt.estComprisEntre(2010, 2011)!=false){
			fail("Entre 2010 et 2011 avec 2012");
		}
		if(evt.estComprisEntre(2010, 2012)!=true){
			fail("Entre 2010 et 2012 avec 2012");
		}
		
		
	}

}
