package Vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import Modele.Chronologie;
import Modele.Evenement;

public class PanelEvenements extends JPanel{

	private CardLayout gestionnaireDeCartes;
	private EtiquetteEvt[] tableauEtiquettesEvts;
	private String[] titresEvts;
	private JButton boutonDefilerGauche;
	private JButton boutonDefilerDroit;
	private JPanel panelDiapoEvts;
	private Chronologie chronologie;
		
		public PanelEvenements(Chronologie parChronologie){
			
			//Chronologie
			chronologie=parChronologie;
			
			//Boutons et leurs actionCommand
			
			boutonDefilerDroit = new JButton(">");
			boutonDefilerGauche = new JButton("<");
			boutonDefilerDroit.setActionCommand("Droite");
			boutonDefilerGauche.setActionCommand("Gauche");
			
			//Cr�ation du diaporama gr�ce � un CardLayout
			
			panelDiapoEvts = new JPanel();
			gestionnaireDeCartes = new CardLayout(5,5);
			panelDiapoEvts.setLayout(gestionnaireDeCartes);
			tableauEtiquettesEvts = new EtiquetteEvt[chronologie.getSizeTabEvt()];//Stockage des �tiquettes
			titresEvts = new String[chronologie.getSizeTabEvt()]; //Stockage des intitul�s
			
			//Remplissage des stockages
			int i =0;
			
			for(int annee : chronologie.getTabEvt().keySet()){
				for(Evenement evt : chronologie.getTabEvt().get(annee)){
					titresEvts[i]=evt.getTitre();
					tableauEtiquettesEvts[i]=new EtiquetteEvt(evt);
					panelDiapoEvts.add(tableauEtiquettesEvts[i], titresEvts[i]);
					i++;
				}
			}
			
			//Layout g�n�ral
			this.setLayout(new BorderLayout());

			//placement des panels
			this.add(boutonDefilerGauche, BorderLayout.WEST);
			this.add(panelDiapoEvts, BorderLayout.CENTER);
			this.add(boutonDefilerDroit, BorderLayout.EAST);
			
			
		}
		
		/**
		 *<font color="red"> cherche dans le tableau d'Etiquettes celle qui est affich�e sur le CardLayout </font>
		 * @return Evenement l'�v�nement dont l'�tiquette est affich�e
		 */
		
		public Evenement estAffiche(){
			for(int i=0;i<tableauEtiquettesEvts.length;i++){
				if(tableauEtiquettesEvts[i].isVisible() == true){
					return tableauEtiquettesEvts[i].getEvenement();
					//Cherche dans le stockage des �tiquettes celle qui est visible dans le CardLayout
				}
			}
			
			return null;
		}
		
		//ActionListener sur les boutons
		
		/**
		 * Met le panel en param�tre � l'�coute des bouton de d�filement gauche et droite
		 * @param panel JPanel
		 */
		
		public void enregistreEcouteur(JPanel panel){
			boutonDefilerDroit.addActionListener((ActionListener) panel);
			boutonDefilerGauche.addActionListener((ActionListener) panel);
		}
		
		//getters
		
		/**
		 * <font color="red">Permet de de r�cup�rer le CardLayout</font>
		 * @return gestionnaireDeCartes CardLayout
		 */
		
		public CardLayout getCardLayout() {
			return gestionnaireDeCartes;
		}
		
		/**
		 * <font color="red">Permet de de r�cup�rer le JPanel qui contient le CardLayout</font>
		 * @return panelDiapoEvts JPanel
		 */

		public JPanel getPanelDiapoEvts(){
			return panelDiapoEvts;
		}

	
}
