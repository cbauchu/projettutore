package Vue;

import java.awt.Component;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CelluleRenderer extends JLabel implements TableCellRenderer{
	public CelluleRenderer() {
		
		//Contruction du renderer
		super();
		setOpaque(true);
		setHorizontalAlignment(JLabel.CENTER);
		
	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object adrImage, boolean estSelectionne, 
			boolean aLeFocus, int ligne, int colonne) {
		
		//cr�e une image � partir de l'adresseImage
		String evenement = (String) adrImage;
		ImageIcon originale= new ImageIcon(evenement);
		
		//redimensionnement
		ImageIcon redimensionnee = new ImageIcon(originale.getImage().getScaledInstance(200, 100, Image.SCALE_DEFAULT));
		
		//Place l'image dans l'objet
		this.setIcon(redimensionnee);
		return this;
	}

}
