package Vue;



import java.awt.BorderLayout;
import java.awt.Rectangle;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import Modele.Chronologie;
import Modele.Evenement;
import Modele.ModeleTable;

public class PanelTable extends JPanel {
	private Chronologie chronologie;
	private JTable tableChronologie;
	private ModeleTable modele;
	
	public PanelTable(Chronologie parChronologie){
		
		//Instantiation de la chronologie
		chronologie=parChronologie;
		
		//cr�ation d'un mod�le pour la table
		modele= new ModeleTable(chronologie);
		
		//Cr�ation de la table
		tableChronologie = new JTable(modele);
		tableChronologie.setDefaultRenderer(String.class, new CelluleRenderer());
		JScrollPane scrollPane = new JScrollPane(tableChronologie, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		//Redimensionnement des colonnes et lignes
		int nombreDeColonnes = modele.getColumnCount();
		for(int j =0; j<nombreDeColonnes; j++) {
			tableChronologie.getColumnModel().getColumn(j).setPreferredWidth(200);
		}
		tableChronologie.setRowHeight(100);
		
		//Interdire les redimensionnements automatiques
		tableChronologie.getTableHeader().setResizingAllowed(false);
		tableChronologie.getTableHeader().setReorderingAllowed(false);
		tableChronologie.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		
		//Placement du layout et du panel
		this.setLayout(new BorderLayout());
		this.add(scrollPane, BorderLayout.CENTER);
		
		
	}
	
	//getter
	/**
	 * <font color="red">Renvoie la JTable contenue dans ce panel</font>
	 * @return tableChronologie JTable
	 */
	public JTable getTableChronologie() {
		return tableChronologie;
	}
	
	
	/**
	 * D�place la scrollbar au niveau de la case de la JTable correspondant
	 * � l'�v�nement entr� en param�tre
	 * @param evt Evenement
	 */
	public void scrollToVisible(Evenement evt) {
		for(int i=0;i<modele.getColumnCount();i++){
			for(int j=0;j<modele.getRowCount();j++){
				if(modele.getValueAt(j, i)!=null && modele.getValueAt(j, i).equals(evt.getAdrImage())){
					Rectangle rect = tableChronologie.getCellRect(j, i, true); //cr�e un rectangle th�orique plac� au niveau de la case de l'�v�nement en param�tre
					tableChronologie.scrollRectToVisible(rect);//placement de la barre au niveau de ce rectangle
				}
			}
		}
		
	}




}
