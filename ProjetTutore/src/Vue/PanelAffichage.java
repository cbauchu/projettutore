package Vue;


import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import Modele.Chronologie;
import Modele.ModeleTable;


public class PanelAffichage extends JPanel implements MouseListener, ActionListener{
	
	private PanelEvenements etiquettesEvenements;
	private PanelTable table;
	private Chronologie chronologie;
	
	public PanelAffichage(Chronologie parChronologie){
		
		//Layout
		this.setLayout(new GridLayout(2, 1));
		
		//Instantiation de la chronologie 
		chronologie = parChronologie;
		
		//Etiquette en haut du panel avec le titre de la chronologie
		JLabel intitule=new JLabel(chronologie.getIntitule(), JLabel.CENTER);
		intitule.setFont(new Font("Serif", Font.BOLD, 20));
		
		//Instantiation des deux panelFils avec pour param�tre la chronologie instanti�e
		etiquettesEvenements = new PanelEvenements(chronologie);
		table = new PanelTable(chronologie);
		
		//MouseListener
		table.getTableChronologie().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				JTable table=(JTable) evt.getSource();
				ModeleTable model=(ModeleTable) table.getModel();
				Point point= evt.getPoint();
				int rowIndex = table.rowAtPoint(point);
				int colIndex = table.columnAtPoint(point);
				if(model.getValueAt(rowIndex, colIndex)!=null) {
						etiquettesEvenements.getCardLayout().show(etiquettesEvenements.getPanelDiapoEvts(),
						model.getHashMapEvenementIntitule().get(model.getValueAt(rowIndex, colIndex)).getTitre());
						
				}//Affiche l'�v�nement correspondant � la case de la table sur laquelle on clique)
			}
		});
		
		
		//Panel haut + Layout
		JPanel panelHaut = new JPanel();
		panelHaut.setLayout(new BorderLayout());
		panelHaut.add(intitule, BorderLayout.NORTH);
		panelHaut.add(etiquettesEvenements, BorderLayout.CENTER);
		
		//Ajout des panels
		this.add(panelHaut);
		this.add(table);
		
		//Se met � l'�coute des boutons gauche et droite permettant de parcourir le diaporama d'�tiquettes des �v�nements
		etiquettesEvenements.enregistreEcouteur(this);
		
		
	}
	
	//getter
	/**
	 * <font color="red">Permet de r�cup�rer le PanelEvenements, panel fils de PanelAffichage</font>
	 * @return etiquettesEvenements PanelEvenement  renvoie un panelEvenement
	 */
	public JPanel getPanelEvenements() {
		return etiquettesEvenements;
	}

	
	//m�thodes de mouseListener
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	//ActionPerformed

	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getActionCommand().equals("Gauche")) {
			etiquettesEvenements.getCardLayout().previous(etiquettesEvenements.getPanelDiapoEvts());
			table.scrollToVisible(etiquettesEvenements.estAffiche());
			//Affiche l'�v�nement pr�c�dent dans le diaporama et scroll la barre jusqu'� l'�v�nement nouvellement affich�
		}
		if(evt.getActionCommand().equals("Droite")) {
			etiquettesEvenements.getCardLayout().next(etiquettesEvenements.getPanelDiapoEvts());
			table.scrollToVisible(etiquettesEvenements.estAffiche());
			//Affiche l'�v�nement suivant dans le diaporama et scroll la barre jusqu'� l'�v�nement nouvellement affich�
		}
		
		
	}

}
