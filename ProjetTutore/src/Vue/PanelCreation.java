package Vue;

import java.awt.CardLayout;
import javax.swing.JPanel;
import Controleur.Controleur;
import Modele.Chronologie;

public class PanelCreation extends JPanel{

	
	private PanelNouvelleFrise panelNouvelleFrise;
	private PanelFriseFichier panelFriseFichier;
	private PanelAjoutEvenement panelAjoutEvenement;
	private CardLayout gestionnaireDeCartes;
	private PanelSupprimerEvenement panelSuppressionEvenement;

	public PanelCreation(){
		
		//Cr�ation des panel fils
		
		panelFriseFichier =new PanelFriseFichier();
		panelNouvelleFrise = new PanelNouvelleFrise();
		panelAjoutEvenement = new PanelAjoutEvenement();
		panelSuppressionEvenement = new PanelSupprimerEvenement();
		
		//Layout
		
		gestionnaireDeCartes = new CardLayout(3,3);
		this.setLayout(gestionnaireDeCartes);
		
		//Ajout des Layout
		
		this.add(panelNouvelleFrise, "Nouvelle Frise");
		this.add(panelFriseFichier, "Charger Frise");
		this.add(panelAjoutEvenement, "Ajouter un �v�nement");
		this.add(panelSuppressionEvenement, "Supprimer un �v�nement");
	}
	
	//getters
	/**
	 * <font color="red">Permet de renvoyer le PanelNouvelleFrise, panel fils de PanelCreation</font>
	 * @return panelNouvelleFrise:PanelNouvelleFrise
	 */
	public PanelNouvelleFrise getPanelNouvelleFrise() {
		return panelNouvelleFrise;
	}
	
	/**
	 * <font color="red">Permet de renvoyer le PanelFriseFichier, panel fils de PanelCreation</font>
	 * @return panelFriseFichier:PanelFriseFichier
	 */
	
	public PanelFriseFichier getPanelFriseFichier() {
		return panelFriseFichier;
	}
	
	/**
	 * <font color="red">Permet de renvoyer le PanelAjoutEvenement, panel fils de PanelCr�ation</font>
	 * @return panelAjoutEvenement PanelAjoutEvenement
	 */
	
	public PanelAjoutEvenement getPanelAjoutEvenement() {
		return panelAjoutEvenement;
	}
	
	/**
	 * <font color="red">Permet de renvoyer le PanelSuppressionEvenement, panel fils de PanelCreation</font>
	 * @return panelSuppressionEvenement PanelSuppresionEvenement
	 */
	
	public PanelSupprimerEvenement getPanelSuppressionEvenement() {
		return panelSuppressionEvenement;
	}
	/**
	 * <font color="red">Permet de renvoyer le CardLayout du panel</font>
	 * @return gestionnaireDeCartes CardLayout
	 */
	public CardLayout getGestionnaireDeCartes() {
		return gestionnaireDeCartes;
	}
	
	//EnregistreEcouteur qui appelle les enregistreEcouteurs des panelFils
	/**
	 * permet de mettre le controleur en param�tre � l'�coute de tous les panels fils
	 * @param parControleur:Controleur
	 */
	public void enregistreEcouteur(Controleur parControleur){
		panelNouvelleFrise.enregistreEcouteur(parControleur);
		panelFriseFichier.enregistreEcouteur(parControleur);
		panelAjoutEvenement.enregistreEcouteur(parControleur);
		panelSuppressionEvenement.enregistreEcouteur(parControleur);
		
	}
}