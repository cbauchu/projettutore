package Vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import Controleur.Controleur;

public class PanelFriseFichier extends JPanel {
	
	private JButton ajouterFriseFichier;
	private JComboBox fichiers;
	private String[] listeFichiers;
	
	public PanelFriseFichier() {
		
		//GridBagLayout
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints contrainteEvenement = new GridBagConstraints();
		contrainteEvenement.insets = new Insets(3,3,3,3);
		
		//Cration d'une etiquette
	
		JLabel nomDuFichier=new JLabel("Nom du fichier");
		
		//Cr�ation d'un fichier qui va indiquer o� chercher les chronologies enregistr�es
		File repertoire = new File("chronologies");
		
		//Tableau instanci� avec les fichiers dans le File d�termin� avant
		listeFichiers = repertoire.list();
		
		//Instantiation d'une JComboBox � partir de ce tableau
		fichiers = new JComboBox(listeFichiers);
		
		//Bouton et ActionCommand
		ajouterFriseFichier=new JButton("Charger");	
		ajouterFriseFichier.setActionCommand("Fichier");
		
		//Ajout d'un Border
		TitledBorder borderFrise;
		borderFrise = BorderFactory.createTitledBorder( "Chargement d'une chronologie d�j� existante");
		borderFrise.setTitleColor(new Color(255,0,0));
		borderFrise.setTitleFont(new Font("Serif", Font.BOLD, 20));
		borderFrise.setTitleJustification(TitledBorder.CENTER);
		this.setBorder(borderFrise);
		
		
		//Ajout des �l�ments dans le GridBagLayout
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=0;
		this.add(nomDuFichier,contrainteEvenement);
		
		contrainteEvenement.gridx=1;
		contrainteEvenement.gridy=0;
		this.add(fichiers,contrainteEvenement);
		
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=1;
		this.add(ajouterFriseFichier,contrainteEvenement);
		
	}
	
	
	//getter
	/**
	 * <font color="red">Renvoie le nom du fichier choisi dans la JComboBox</font>
	 * @return String
	 */
	public String getNomFichier(){
		int i = fichiers.getSelectedIndex();
		return (String) fichiers.getItemAt(i);
		
	}
	
	//ActionListener sur le bouton ajouter
	/**
	 * Met le controleur en param�tre � l'�coute du bouton Charger 
	 * @param parControleur
	 */
	public void enregistreEcouteur(Controleur parControleur){
		ajouterFriseFichier.addActionListener(parControleur);
	}
}
