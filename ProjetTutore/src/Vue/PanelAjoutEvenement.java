package Vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import Controleur.Controleur;

public class PanelAjoutEvenement extends JPanel implements ActionListener{
	
private JButton ajouterEvenement;
	
	private JTextField dateEvt;
	private JFileChooser chooser;
	private JTextArea description;
	private JTextField titreEvenement;
	private String[] etiquettePoids ={"1","2","3","4"};
	private JComboBox poids;
	private JComboBox joursEvt;
	private JComboBox moisEvt;
	private JLabel choixImage;
	
	public PanelAjoutEvenement() {
		
		//Instantiation des champs
		poids = new JComboBox(etiquettePoids);
		
		description = new JTextArea(6,14);
		description.setLineWrap(true); //Evite le redimensionnement automatique de la JTestArea
		titreEvenement= new JTextField(12);
		dateEvt = new JTextField(4);
		ajouterEvenement=new JButton("Ajouter");	
		choixImage = new JLabel("Aucun fichier choisi");
		
		//Etiquettes et boutons
		JLabel textePoids=new JLabel("Poids");
		JLabel dateEvenement =new JLabel("Date");
		JLabel titreEvt = new JLabel("Titre de l'�venement");
		JLabel adresseImage =new JLabel("Adresse de l'image");
		JLabel descriptionEvt  =new JLabel("Description de l'evenement");
		JButton parcourir=new JButton("Parcourir");
		
		//Boutons et actions
		parcourir.setActionCommand("Parcourir");
		parcourir.addActionListener(this);
		ajouterEvenement.setActionCommand("Evenement");
		
		//Pour les JComboBox
		String[] listeJours = new String[31];
		String[] listeMois = new String[12];
				
		//Parcours des dossiers pour l'adresse Image d'un �v�nement
		
		chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new File("."));
	    chooser.setDialogTitle("Choisir une image");
	    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); // s�lectionner dossier et fichiers
	    chooser.setAcceptAllFileFilterUsed(false); 
	    
	     
		
		//remplissage de la boucle des jours fin
		
		for(int k =0; k<31 ;k++){
			listeJours[k]=Integer.toString(k+1);
		}
		
		joursEvt = new JComboBox(listeJours);
				
		//remplissage de la boucle des mois fin
				
		for(int l =0; l<12 ;l++){
			listeMois[l]=Integer.toString(l+1);
		}
		
		moisEvt = new JComboBox(listeMois);
		
		// Layout
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints contrainteEvenement = new GridBagConstraints();
		contrainteEvenement.insets = new Insets(3,3,3,3);
		
		
		this.add(ajouterEvenement);
		
		
		
		
		//Ajout d'un Border
		
		TitledBorder borderFrise;
		borderFrise = BorderFactory.createTitledBorder( "Ajout d'un �v�nement � la frise");
		borderFrise.setTitleColor(new Color(255,0,0));
		borderFrise.setTitleFont(new Font("Serif", Font.BOLD, 20));
		borderFrise.setTitleJustification(TitledBorder.CENTER);
		this.setBorder(borderFrise);
		
		//Ajout des �l�ments dans le GridBagLayout
		
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=0;
		contrainteEvenement.gridwidth=2;
		contrainteEvenement.anchor=GridBagConstraints.WEST;
		this.add(dateEvenement,contrainteEvenement);
		
		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=0;
		contrainteEvenement.gridwidth=1;
		this.add(joursEvt,contrainteEvenement);
		
		contrainteEvenement.gridx=3;
		contrainteEvenement.gridy=0;
		this.add(moisEvt,contrainteEvenement);
		
		contrainteEvenement.gridx=4;
		contrainteEvenement.gridy=0;
		this.add(dateEvt,contrainteEvenement);
		
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=2;
		contrainteEvenement.gridwidth=2;
		this.add(titreEvt,contrainteEvenement);
		
		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=2;
		contrainteEvenement.gridwidth=14;
		this.add(titreEvenement,contrainteEvenement);
		
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=4;
		contrainteEvenement.gridwidth=2;
		this.add(textePoids,contrainteEvenement);

		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=4;
		contrainteEvenement.gridwidth=1;
		this.add(poids,contrainteEvenement);
		
		
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=6;
		contrainteEvenement.gridwidth=2;
		this.add(descriptionEvt,contrainteEvenement);
		
		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=6;
		contrainteEvenement.gridwidth=10;
		contrainteEvenement.gridheight=10;
		this.add(description,contrainteEvenement);
		
		contrainteEvenement.gridx=0;
		contrainteEvenement.gridy=16;
		contrainteEvenement.gridwidth=2;
		contrainteEvenement.gridheight=1;
		this.add(adresseImage,contrainteEvenement);
		
		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=16;
		contrainteEvenement.gridwidth=2;
		this.add(parcourir,contrainteEvenement);
		
		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=17;
		contrainteEvenement.gridwidth=20;
		this.add(choixImage,contrainteEvenement);
		
		contrainteEvenement.gridx=2;
		contrainteEvenement.gridy=18;
		contrainteEvenement.gridwidth=2;
		this.add(ajouterEvenement,contrainteEvenement);
		
		
	}
	
	//getters
	
	/**
	 * <font color="red">>Permet de renvoyer ce que l'utilisateur a �crit dans le JTextfield dateEvt</font>
	 * @return String renvoie une String
	 */
	public String getDateEvt(){
		return dateEvt.getText();
	}
	/**
	 * <font color="red">Permet de renvoyer ce que l'utilisateur a choisi dans la JComboBox moisEvt</font>
	 * @return Int renvoie un Int correspondant au numero du mois
	 */
	public int getMoisEvt() {
		return moisEvt.getSelectedIndex() +1;
	}
	/**
	 * <font color="red">Permet de renvoyer ce que l'utilisateur a choisi dans la JComboBox joursEvt</font>
	 * @return Int renvoie un Int correspondant au num�ro du jour
	 */
	public int getJourEvt() {
		return joursEvt.getSelectedIndex() +1;
	}
	/**
	 * <font color="red">Permet de renvoyer l'adresse de l'image que l'utilisateur a choisi sous forme de String</font>
	 * @return String renvoie une String correspondant au chemin absolu du fichier choisi
	 */
	public String getAdrImg(){
		return chooser.getSelectedFile().getAbsolutePath();
	}
	/**
	 * <font color="red">Permet de renvoyer ce que l'utilisateur a �crit dans le JTextfield description</font>
	 * @return String renvoie une String
	 */
	public String getDescription(){
		return description.getText();
	}
	/**
	 * <font color="red">Permet de renvoyer ce que l'utilisateur a �crit dans le JTextfield titreEvenement</font>
	 * @return String renvoie une String
	 */
	public String getTitreEvenementt(){
		return titreEvenement.getText();
	}
	/**
	 * <font color="red"><font color="red">Permet de renvoyer le poids que l'utilisateur a choisi </font>
	 * @return int renvoie un Int
	 */
	public int getPoids(){
		return poids.getSelectedIndex() +1;
	}
	
	//enregistre ActionListener sur le bouton ajouter
	/**
	 * Met le controleur en param�tre � l'�coute du bouton Ajouter 
	 * @param parControleur Controleur
	 */
	public void enregistreEcouteur(Controleur parControleur){
		ajouterEvenement.addActionListener(parControleur);
	}
	
	/**
	 * <font color="red">Vide les JTextField et JTextArea, remet les JComboBox � z�ro et met le fichier image choisi � null</font>
	 */

	
	public void viderFormulaire(){
		dateEvt.setText("");
		description.setText("");
		titreEvenement.setText("");
		chooser.setSelectedFile(null);
		joursEvt.setSelectedIndex(0);
		moisEvt.setSelectedIndex(0);
		choixImage.setText("Aucun fichier choisi");
	}

	//Gestion du bouton Parcourir
	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getActionCommand().equals("Parcourir")){
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
				choixImage.setText(chooser.getSelectedFile().getAbsolutePath()); //Affiche le chemin absolu du
				//fichier choisi dans un label � c�t� du bouton parcourir. Si rien n'est choisi, l'�tiquette affice "Aucun fichier choisi"
			      }
			   
		}
		
	}
}
