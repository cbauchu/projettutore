package Vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import Controleur.Controleur;



public class PanelNouvelleFrise extends JPanel {
	
	private JButton ajouterNouvelleFrise;
	private JTextField dateDebutTexte;
	private JTextField dateFinTexte;
	private JTextField periodeTexte;
	private JTextField intituleTexte;
	private JTextField fichierTexte;
	
	public PanelNouvelleFrise() {
		
		//etiquettes
		
		JLabel dateDebut=new JLabel("Ann�e de d�but");
		JLabel periode=new JLabel("P�riode");
		JLabel dateFin=new JLabel("Ann�e de fin");
		JLabel intitule=new JLabel("Intitul�");
		JLabel fichier=new JLabel("Nom du fichier");
		
		//Instantiation des champs
		dateDebutTexte = new JTextField(10);
		dateFinTexte = new JTextField(10);
		periodeTexte = new JTextField(10);
		intituleTexte = new JTextField(10);
		fichierTexte = new JTextField(10);
		
		//Bouton et ActionCommand
		ajouterNouvelleFrise= new JButton("Cr�er");
		ajouterNouvelleFrise.setActionCommand("NouvelleFrise");
		
		//Ajout d'un border
		TitledBorder borderFrise;
		borderFrise = BorderFactory.createTitledBorder( "Cr�ation d'une Frise chronologique");
		borderFrise.setTitleColor(new Color(255,0,0));
		borderFrise.setTitleFont(new Font("Serif", Font.BOLD, 20));
		borderFrise.setTitleJustification(TitledBorder.CENTER);
		this.setBorder(borderFrise);
		
		//Layout
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints contrainteFrise = new GridBagConstraints();
		contrainteFrise.insets = new Insets(3,3,3,3);
		
		//Ajout des �l�ments dans le gridBagLayout
		
		contrainteFrise.gridx=0;
		contrainteFrise.gridy=0;
		contrainteFrise.anchor=GridBagConstraints.WEST;
		this.add(dateDebut,contrainteFrise);
		
		contrainteFrise.gridx=2;
		contrainteFrise.gridy=0;
		this.add(dateDebutTexte,contrainteFrise);
		
		contrainteFrise.gridx=0;
		contrainteFrise.gridy=1;
		this.add(dateFin,contrainteFrise);
		
		contrainteFrise.gridx=2;
		contrainteFrise.gridy=1;
		this.add(dateFinTexte,contrainteFrise);
		
		contrainteFrise.gridx=0;
		contrainteFrise.gridy=4;
		this.add(periode,contrainteFrise);
		
		contrainteFrise.gridx=2;
		contrainteFrise.gridy=4;
		this.add(periodeTexte,contrainteFrise);
		
		contrainteFrise.gridx=0;
		contrainteFrise.gridy=6;
		this.add(intitule,contrainteFrise);
		
		contrainteFrise.gridx=2;
		contrainteFrise.gridy=6;
		this.add(intituleTexte,contrainteFrise);
		
		contrainteFrise.gridx=0;
		contrainteFrise.gridy=8;
		this.add(fichier,contrainteFrise);
		
		contrainteFrise.gridx=2;
		contrainteFrise.gridy=8;
		this.add(fichierTexte,contrainteFrise);
		
		contrainteFrise.gridx=10;
		contrainteFrise.gridy=10;
		this.add(ajouterNouvelleFrise,contrainteFrise);
		
	}
	
	//getters
	
	/**
	 * <font color="red">Renvoie le contenu du JTextField dateDebutTexte</font>
	 * @return String 
	 */
	public String getDateDebut(){
		return dateDebutTexte.getText();
	}
	/**
	 * <font color="red">Renvoie le contenu du JTextField dateFinTexte</font>
	 * @return String 
	 */
	public String getDateFin(){
		return dateFinTexte.getText();
	}
	/**
	 * <font color="red">Renvoie le contenu du JTextField periodeTexte</font>
	 * @return String 
	 */
	public String getPeriode(){
		return periodeTexte.getText();
	}
	/**
	 * <font color="red">Renvoie le contenu du JTextField intituleTexte</font>
	 * @return String 
	 */
	public String getIntitule(){
		return intituleTexte.getText();
	}
	/**
	 * <font color="red">Renvoie le contenu du JTextField fichierTexte</font>
	 * @return String 
	 */
	public String getFichier(){
		return fichierTexte.getText();
	}
	
	/** met les champs � "String vide"
	 * 
	 */
	
	public void viderFormulaire(){
		dateDebutTexte.setText("");
		dateFinTexte.setText("");
		periodeTexte.setText("");
		intituleTexte.setText("");
		fichierTexte.setText("");
	}
	
	//ActionListener sur le bouton ajouter
	/**
	 * Met le controleur en param�tre � l'�coute du bouton Cr�er
	 * @param parControleur
	 */
	public void enregistreEcouteur(Controleur parControleur){
		ajouterNouvelleFrise.addActionListener(parControleur);
	}
}
