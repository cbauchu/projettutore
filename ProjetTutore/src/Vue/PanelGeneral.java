package Vue;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Controleur.Controleur;
import Modele.Chronologie;

public class PanelGeneral extends JPanel implements ActionListener {
	
	private String[] intitulesPanneaux = {"Cr�ation", "Affichage", "Quitter"};
	String[] itemsCreation = {"Nouvelle Frise", "Charger Frise", "Ajouter un �v�nement", "Supprimer un �v�nement"};
	private CardLayout gestionnaireCartes;
	Controleur controleur;
	private Chronologie chronologie;
	private PanelAffichage panelAffichage;
	private PanelCreation panelCreation;
	
	public PanelGeneral(){
		
		//Instantiation de la chronologie
		chronologie=new Chronologie(); 
		
		//Layout : Empilement de tous les panels
		gestionnaireCartes=new CardLayout(2,2);
		this.setLayout(gestionnaireCartes);
		
		//Ajout des deux panels empil�s
		
		panelCreation=new PanelCreation();
		this.add(panelCreation, intitulesPanneaux[0]);
		panelAffichage = new PanelAffichage(chronologie);
		controleur= new Controleur(panelCreation, this);
		this.add(panelAffichage, intitulesPanneaux[1]);
		
		
	}
	
	/**
	 * <font color="red">Permet de changer la chronologie du champ chronologie puis recharge le panelAffichage avec la nouvelle chronologie</font>
	 * @param parChronologie Chronologie � remplacer
	 */
	
	public void setChronologie(Chronologie parChronologie){
		chronologie=parChronologie;
		panelAffichage=new PanelAffichage(chronologie);
		this.add(panelAffichage, intitulesPanneaux[1]);
	}
	
	//getter
	/**
	 * <font color="red">Permet de de r�cup�rer la chronologie</font>
	 * @return chronologie Chronologie
	 */
	public Chronologie getChronologie(){
		return chronologie;
	}
	
	public void actionPerformed(ActionEvent evt){
		if(evt.getActionCommand() == "Nouvelle Frise"){
			gestionnaireCartes.show(this, intitulesPanneaux[0]);
			panelCreation.getGestionnaireDeCartes().show(panelCreation, itemsCreation[0]);
			//Montre d'abord le panelCreation puis get son cardLayout pour afficher le bon panel fils de panelCreation
		}
		
		else if(evt.getActionCommand() == "Charger Frise"){
			gestionnaireCartes.show(this, intitulesPanneaux[0]);
			panelCreation.getGestionnaireDeCartes().show(panelCreation, itemsCreation[1]);
			//Montre d'abord le panelCreation puis get son cardLayout pour afficher le bon panel fils de panelCreation
		}
		
		else if(evt.getActionCommand() == "Ajouter un �v�nement"){
			gestionnaireCartes.show(this, intitulesPanneaux[0]);
			panelCreation.getGestionnaireDeCartes().show(panelCreation, itemsCreation[2]);
			//Montre d'abord le panelCreation puis get son cardLayout pour afficher le bon panel fils de panelCreation
		}
		
		else if(evt.getActionCommand() == "Supprimer un �v�nement"){
			gestionnaireCartes.show(this, intitulesPanneaux[0]);
			panelCreation.getGestionnaireDeCartes().show(panelCreation, itemsCreation[3]);
			//Montre d'abord le panelCreation puis get son cardLayout pour afficher le bon panel fils de panelCreation
		}
		
		
		else if(evt.getActionCommand() == "Affichage"){
			gestionnaireCartes.show(this, intitulesPanneaux[1]);
		}
		else if(evt.getActionCommand() == "Quitter"){
			
			JOptionPane confirmation=new JOptionPane();
		
			int code= confirmation.showConfirmDialog(null, "Voulez vous vraiment quitter?","Arret du programme",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
			if(code== JOptionPane.OK_OPTION) {
			System.exit(code);
			} // Pop up avec confirmation du choix
			
		}
		else if(evt.getActionCommand() == "?"){
			JOptionPane information=new JOptionPane();
			information.showMessageDialog(null,"Informations � propos de notre projet\n\n Ce projet a �t� cr�� par Claire Bauchu \net Louis Bizot dans le cadre du projet\n de fin de 1ere ann�e.\n\n 07/06/2018","Information",JOptionPane.INFORMATION_MESSAGE);
			//pop up avec infos � propos du programme
		}
	}
}
