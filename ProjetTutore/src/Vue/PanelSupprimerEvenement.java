package Vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import Controleur.Controleur;

public class PanelSupprimerEvenement extends JPanel {

		private JButton supprimerEvenement;
		private JTextField nomEvtTexte;
		
		public PanelSupprimerEvenement() {
			
			//Instantiation des champs
			supprimerEvenement = new JButton("Supprimer");
			nomEvtTexte = new JTextField(10);
			
			//actionCommand du bouton supprimer
			supprimerEvenement.setActionCommand("Supprimer un �v�nement");
			
			//�tiquette
			JLabel evenementEtiquette = new JLabel("Titre de l'�v�nement");
			
			//Layout
			this.setLayout(new GridBagLayout());
			GridBagConstraints contrainteEvenement = new GridBagConstraints();
			contrainteEvenement.insets = new Insets(3,3,3,3);
			
			
			//Ajout d'un border
			TitledBorder borderFrise;
			borderFrise = BorderFactory.createTitledBorder("Supprimer un �v�nement");
			borderFrise.setTitleColor(new Color(255,0,0));
			borderFrise.setTitleFont(new Font("Serif", Font.BOLD, 20));
			borderFrise.setTitleJustification(TitledBorder.CENTER);
			this.setBorder(borderFrise);
			
			
			//Ajout des �l�ments dans le GridBagLayout
			contrainteEvenement.gridx=0;
			contrainteEvenement.gridy=0;
			this.add(evenementEtiquette,contrainteEvenement);
			
			contrainteEvenement.gridx=1;
			contrainteEvenement.gridy=0;
			this.add(nomEvtTexte,contrainteEvenement);
			
			contrainteEvenement.gridx=0;
			contrainteEvenement.gridy=1;
			this.add(supprimerEvenement,contrainteEvenement);
			
			
			
		}
		
		/**met le champ de texte � "String vide"
		 * 
		 */

		public void viderFormulaire(){
			nomEvtTexte.setText("");
		}
		
		//getter
		/**
		 * <font color="red">Renvoie le nom de l'�venement � supprimer sous forme de String</font>
		 * @return nomEvtTexte.getText() String
		 */
		public String getNomEvt(){
			return nomEvtTexte.getText();
		}
		
		//actionListener sur le bouton supprimer
		/**
		 * Met le controleur en param�tre � l'�coute du bouton Supprimer
		 * @param parControleur Controleur
		 */
		public void enregistreEcouteur(Controleur parControleur){
			supprimerEvenement.addActionListener(parControleur);
		}
}
