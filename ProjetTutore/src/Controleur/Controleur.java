package Controleur;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Modele.Date;

import java.io.File;
import java.lang.Integer;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import Modele.Chronologie;
import Modele.Evenement;
import Modele.LectureEcriture;
import Vue.PanelAffichage;
import Vue.PanelAjoutEvenement;
import Vue.PanelCreation;
import Vue.PanelFriseFichier;
import Vue.PanelGeneral;
import Vue.PanelNouvelleFrise;
import Vue.PanelSupprimerEvenement;

public class Controleur implements ActionListener{
	
	private PanelCreation panelCreation;
	private PanelGeneral panelGeneral;
	private PanelNouvelleFrise panelNouvelleFrise;
	private PanelFriseFichier panelFriseFichier;
	private PanelAjoutEvenement panelAjoutEvenement;
	private PanelSupprimerEvenement panelSupprimerEvenement;
	
	public Controleur(PanelCreation parPanelCreation, PanelGeneral parPanelGeneral){
		
		//Instantiation des panels en param�tre
		panelCreation=parPanelCreation;
		panelGeneral=parPanelGeneral;
		
		//R�cup�ration des panel Fils de panelCreation gr�ce � ses getters
		panelNouvelleFrise = panelCreation.getPanelNouvelleFrise();
		panelFriseFichier = panelCreation.getPanelFriseFichier();
		panelAjoutEvenement=panelCreation.getPanelAjoutEvenement();
		panelSupprimerEvenement=panelCreation.getPanelSuppressionEvenement();
		
		//EnregistreEcouteur sur les panelFils de cr�ation
		panelCreation.enregistreEcouteur(this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		
		if(evt.getActionCommand().equals("NouvelleFrise")){
				
				//Cr�ation d'une nouvelle chronologie
				Chronologie chronologie = new Chronologie(new Date(01,01,Integer.parseInt(panelNouvelleFrise.getDateDebut())), 
						new Date(31,12,Integer.parseInt(panelNouvelleFrise.getDateFin())), Integer.parseInt(panelNouvelleFrise.getPeriode()), panelNouvelleFrise.getIntitule(), 
						panelNouvelleFrise.getFichier());
				
				if(chronologie.estValide()) { //v�rification de sa validit�
					
					//�criture dans un fichier de la chronologie
					File fichier = new File("chronologies"+File.separator+panelNouvelleFrise.getFichier()+".ser");
					LectureEcriture.ecriture(fichier, chronologie);
					
					//Mise en place dans la vue de la nouvelle chronologie
					panelGeneral.setChronologie(chronologie);
					
					//Vidage du formulaire
					panelNouvelleFrise.viderFormulaire();
				}
				
				else {//en cas de non validit�, pop up d'erreur
					ImageIcon originale= new ImageIcon("images"+File.separator+"erreur.png");
					ImageIcon redimensionnee = new ImageIcon(originale.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
					JOptionPane.showMessageDialog(panelNouvelleFrise, "La chronologie n'est pas valide. \nVeuillez entrer une ann�e de \nd�but inf�rieure � celle de fin.", "Erreur", 1, redimensionnee);
				}
		}
		
		if(evt.getActionCommand().equals("Fichier")){ // charger une chronologie depuis un fichier
			
			//mise en place de la nouvelle chronologie dans la vue � partir de la lecture du fichier choisi par l'utilisateur
			File fichier = new File("chronologies"+File.separator+panelFriseFichier.getNomFichier());
			panelGeneral.setChronologie((Chronologie) LectureEcriture.lecture(fichier));
		}
		
		if(evt.getActionCommand().equals("Evenement")){ // ajout d'un nouvel �v�nement
			
			//r�cup�ration de la chronologie actuelle
			Chronologie tempoChrono = panelGeneral.getChronologie();
			
			//Cr�ation d'une date temporaire pour l'�v�nement � partir des infos entr�es par l'utilisateur
			Date tempoDate = new Date(panelAjoutEvenement.getJourEvt(),panelAjoutEvenement.getMoisEvt(),Integer.parseInt(panelAjoutEvenement.getDateEvt()));
			
			if(tempoDate.estValide()) {// En cas de validit� de la date
				
				//Cr�ation d'un �v�nement temporaire
				Evenement tempoEvt = new Evenement(tempoDate, panelAjoutEvenement.getTitreEvenementt(), panelAjoutEvenement.getAdrImg(), panelAjoutEvenement.getDescription(), panelAjoutEvenement.getPoids());
				
				
				if(tempoEvt.estComprisEntre(tempoChrono.getDateDebut().getAnnee(), tempoChrono.getDateFin().getAnnee())) {
					
					//En cas de validit� de l'�v�nement par rapport aux bornes de la chronologie, ajout de l'�v�nement � la chronologie
					tempoChrono.ajout(tempoEvt);
					
					//Mise en place de la chronologie avec le nouvel Ev�nement dans la vue
					panelGeneral.setChronologie(tempoChrono);
					
					//Ecriture dans le fichier de la chronologie
					File fichier = new File("chronologies"+File.separator+panelGeneral.getChronologie().getAdresseFichier()+".ser");
					LectureEcriture.ecriture(fichier, tempoChrono);
					
					//Vidage du formulaire
					panelAjoutEvenement.viderFormulaire();
				}
				
				else { // En cas de non validit� de l'�v�nement, popup avec message d'erreur
					ImageIcon originale= new ImageIcon("images"+File.separator+"erreur.png");
					ImageIcon redimensionnee = new ImageIcon(originale.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
					JOptionPane.showMessageDialog(panelAjoutEvenement, "L'�v�nement n'est pas valide.\nEntrez un �v�nement compatible avec votre\nchronologie", "Erreur", 1, redimensionnee);
				}
				
				
			}
			else { // En cas de non validit� de la date, popup avec message d'erreur
				ImageIcon originale= new ImageIcon("images"+File.separator+"erreur.png");
				ImageIcon redimensionnee = new ImageIcon(originale.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
				JOptionPane.showMessageDialog(panelAjoutEvenement, "L'�v�nement n'est pas valide.\nEntrez une date valide", "Erreur", 1, redimensionnee);
			}
		}
		
		if(evt.getActionCommand().equals("Supprimer un �v�nement")) { // suppression d'un �v�nement
			
			//r�cup�ration de la chronologie actuelle
			Chronologie tempoChrono=panelGeneral.getChronologie();
			
			//parcours de celle-ci jusqu'� trouver le titre de l'�v�nement � supprimer entr� par l'utilisateur
			for(int annee : tempoChrono.getTabEvt().keySet()){
				for(Evenement evenement : tempoChrono.getTabEvt().get(annee)){
					if(evenement.getTitre().equals(panelSupprimerEvenement.getNomEvt())){
						//suppression de l'�v�nement
						tempoChrono.supprimerEvenement(evenement);
					}
				}
			}
			
			//mise en place de la nouvelle chronologie dans la vue
			
			panelGeneral.setChronologie(tempoChrono);
			
			//�criture dans le fichier
			File fichier = new File("chronologies"+File.separator+panelGeneral.getChronologie().getAdresseFichier()+".ser");
			LectureEcriture.ecriture(fichier, tempoChrono);
			
			//vidage du formulaire
			panelCreation.getPanelSuppressionEvenement().viderFormulaire();
		}
	}

}