package Modele;

import java.io.Serializable;

public class Evenement implements Serializable, Comparable <Evenement>{
	private Date chDate;
	private String chTitre;
	private String chAdresseImage;
	private String chDescription;
	private int chPoids;
	
	
	
	
	//Constructeur
	
	public Evenement(Date parDate, String parTitre, String parAdresseFichier, String parDescription, int parPoids) {
		chDate=parDate;
		chTitre=parTitre;
		chAdresseImage=parAdresseFichier;
		chDescription = parDescription;
		chPoids=parPoids;
	}
	
	public Evenement() {
		chDate=new Date();
		chTitre="";
		chAdresseImage="";
		chDescription = "";
		chPoids=1;
	}
	
	/**
	 * <font color="red">Permet d'afficher un �v�nement sous forme titre + date +adresse de l'image associ� + description + poids</font>
	 * @return String renvoie une chaine de caract�res
	 */
	
	public String toString() {
		return chTitre + " " + chDate + " " + chAdresseImage + " " + chDescription + " "+chPoids ;
	}
	
	//Accesseurs
	
	/**
	 * <font color="red">Renvoie la date de l'�v�nement</font>
	 * @return Date 
	 */
	
	public Date getDate() {
		return chDate;
	}
	/**
	 * <font color="red">Renvoie le titre de l'�v�nement</font>
	 * @return String 
	 */
	public String getTitre() {
		return chTitre;
	}
	/**
	 * <font color="red">Renvoie le poids de l'�v�nement</font>
	 * @return int
	 */
	
	public int getPoids() {
		return chPoids;
	}
	/**
	 * <font color="red">Renvoie la description de l'�v�nement</font>
	 * @return String 
	 */
	public String getDescription() {
		return chDescription;
	}
	/**
	 * <font color="red">Renvoie l'adresse de l'image associ�e � l'�v�nement</font>
	 * @return String 
	 */
	public String getAdrImage() {
		return chAdresseImage;
	}
	
	
	/**
	 * <font color="red">Renvoie un int (0 si l'�venement pass� en param�tre a la meme valeur de champ ann�e , -1 si l'�venement pass� en param�tre a une valeur de champ ann�e</font>
	 * inf�rieure, 1 si l'�venement pass� en param�tre a une valeur de champ ann�e sup�rieure)
	 * @param Evenement un �venement pour comparer � celui qui appelle la m�thode
	 * @return Int 
	 */
	
	public int compareTo(Evenement parEven){
		
		int retour=Integer.compare(this.chDate.getAnnee(),parEven.chDate.getAnnee());
		if(retour==0){
			return(Integer.compare(this.chPoids, parEven.chPoids));
			
		}
		else 
			return retour;
	}
	/**
	 * <font color="red">Renvoie un bool�en:True si le champ ann�e de l'�venement est compris entre les deux ann�es pass�es en param�tre , false sinon</font>
	 * @param annee1
	 * @param annee2
	 * @return Boolean 
	 */
	public boolean estComprisEntre(int annee1, int annee2) {
		return (chDate.getAnnee()>=annee1 && chDate.getAnnee()<=annee2);
	}
		
}
