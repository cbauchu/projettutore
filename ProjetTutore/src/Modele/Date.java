package Modele;
import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Calendar;
 
public class Date implements Comparable <Date>, Serializable {
  private int jour;
  private int mois;
  private int annee;
   
  public Date ()   { 
	  GregorianCalendar dateAuj = new GregorianCalendar ();
	  annee = dateAuj.get (Calendar.YEAR);
	  mois = dateAuj.get (Calendar.MONTH)+1; // janvier = 0, fevrier = 1...
	  jour = dateAuj.get (Calendar.DAY_OF_MONTH);
	 
  }
  
  public Date (int parJour, int parMois, int parAnnee)   {   
	jour = parJour;
	mois = parMois;
	annee = parAnnee;		
  } 
   
  /**
  
  * <font color="red">retourne 0 si this et parDate sont egales, 
     
  * -1 si this precede parDate,
     
  *  1 si parDate precede this</font>
     
  *  @param Date prend en param�tre une date pour comparer avec la date qui appelle la m�thode
     
  *  @return Int renvoie un int
     */
  
  public int compareTo (Date parDate) {
    if (annee < parDate.annee)
		return -1;
	if (annee > parDate.annee)
		return 1;
	// les annees sont =
	if (mois < parDate.mois)
		return -1;
	if (mois > parDate.mois)
		return 1;
	// les mois sont =
	if (jour < parDate.jour)
		return -1;
	if (jour > parDate.jour)
		return 1;
	return 0;	
  }
  
  /**
  
  *<font color="red"> Renvoie un int qui, � partir du mois et de l'ann�e indique si le dernier jour du mois est 29,28 ou 31</font>
     
  * @param parMois Int  prend en param�tre le mois pour connaitre le dernier jour du mois (qui d�pend du mois)
     
  * @param parAnnee Int  prend en param�tre l'ann�e pour connaitre le dernier jour du mois (qui d�pend aussi de l'ann�e)
    
  * @return Int renvoie un int
     
  */
 
  
  public static int dernierJourDuMois (int parMois, int parAnnee) {
		switch (parMois) {
			 case 2 : if (estBissextile (parAnnee))  return 29 ; else return 28 ;  
			 case 4 : 	 case 6 : 	 case 9 : 	 case 11 : return 30 ;
			 default : return 31 ;
			}  // switch
	  } 

  /**
  
  *<font color="red"> Permet de savoir si une ann�e et bissextile ou non (True si vrai , False sinon)</font>
     
  * @param parAnnee Int prend en param�tre l'ann�e pour savoir si celle-ci est bissextile
    
   * @return Boolean renvoie un bool�en
     */
  
  private static boolean estBissextile(int parAnnee) {
			return parAnnee % 4 == 0 && (parAnnee % 100 != 0 || parAnnee % 400 == 0);
	  }
  
  /**
  
  * <font color="red">Permet d'afficher sous forme jour + le mois en toute lettre une date</font>
     
  * @return String renvoie une chaine de charact�re
     */
    
  public String toString () {
    String chaine = new String();
   
	chaine += jour + " ";
	switch (mois) {
		 case 1: chaine += "janvier"; break;
		 case 2: chaine += "fevrier"; break;
		 case 3: chaine += "mars"; break;
		 case 4: chaine += "avril"; break;
		 case 5: chaine += "mai"; break;
		 case 6: chaine += "juin"; break;
		 case 7: chaine += "juillet"; break;
		 case 8: chaine += "aout"; break;
		 case 9: chaine += "septembre"; break;
		 case 10: chaine += "octobre"; break;
		 case 11: chaine += "novembre"; break;
		 case 12: chaine += "decembre"; break;
		}	
	chaine+=" " +annee;
	return chaine;
  }  
  
  /**

   * <font color="red">Permet de savoir si une date est sous un format valide : l'ann�e sup�rieure � 0 , le mois compris entre 1 et 12 , le jour compris entre le premier jour du mois et le dernier
    
 * (celui-ci pouvant varier en fonction du mois)</font>
    
 * @return Boolean renvoie un bool�en (True , False)
    */
  
  public boolean estValide() {
	  if(annee>0) {
		  if(mois>0 && mois<=12) {
			  if(jour>0 && jour<=Date.dernierJourDuMois(mois, annee)) {
				  return true;
			  }
			  
		  }
	  }
	  
	  return false;
  }

  /**
   * <font color="red">Renvoie l'ann�e correspondant � l'ann�e d'une date , ou d'un �v�nement</font>
   
  * @return Int renvoie un num�ro d'ann�e
   */
  
  public int getAnnee() { 
	return annee;
}
  
  /**
  
* <font color="red">Renvoie le jour correspondant au jour d'une date , ou d'un �v�nement</font>
   
* @return Int renvoie un num�ro de jour
   */

public int getJour() { 
	return jour;
}

/**

* <font color="red">Renvoie le mois correspondant au mois d'une date , ou d'un �v�nement</font>
 
* @return Int renvoie un num�ro de mois 
 */

public int getMois() { 
	return mois;
}


}