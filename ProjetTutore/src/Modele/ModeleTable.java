package Modele;

import java.util.HashMap;
import java.util.TreeSet;

import javax.swing.table.DefaultTableModel;

public class ModeleTable extends DefaultTableModel{
	
	Chronologie chronologie;
	String[] intitulesColonnes;
	HashMap<String, Evenement> hashMapEvenementIntitule;
	
	public ModeleTable(Chronologie parChronologie){

		chronologie=parChronologie;
		hashMapEvenementIntitule=new HashMap<String, Evenement>();
		
		setColumnCount(chronologie.getDateFin().getAnnee()-chronologie.getDateDebut().getAnnee());
		setRowCount(4);			
		
		setModeleTable(chronologie);
	}
	
	/**
	 * <font color="red">Permet de d�finir la JTable � partir d'une Chronologie ( on utilise les champs de la Chronologie comme la date de d�but et de fin ainsi que les �venements qu'elle contient pour les placer dans la JTable)</font>
	 * @param parChronologie Chronologie: prend en param�tre une chronologie pour utiliser ses champs et l'utiliser pour cr�er la JTable
	 */

	public void setModeleTable(Chronologie parChronologie){
		
		chronologie=parChronologie;

		int firstYear = chronologie.getDateDebut().getAnnee();
		intitulesColonnes = new String[chronologie.getDateFin().getAnnee()-chronologie.getDateDebut().getAnnee()+1];
		
		for(int i=0; i<intitulesColonnes.length; i++){
			
			if(i%chronologie.getPeriode()==0){
				intitulesColonnes[i]=Integer.toString(firstYear+i);
			}
			
			else {
				intitulesColonnes[i]="";
			}
			
		}
		
		intitulesColonnes[intitulesColonnes.length-1]=Integer.toString(chronologie.getDateFin().getAnnee());
		
		
		this.setColumnIdentifiers(intitulesColonnes);
		
		
		
		for(int annee : chronologie.getTabEvt().keySet()) {
			if(chronologie.getTabEvt().get(annee).size()!=0){
				for(Evenement evt : chronologie.getTabEvt().get(annee)){
					setValueAt(evt.getAdrImage(), 3-(evt.getPoids()-1), (intitulesColonnes.length-(chronologie.getDateFin().getAnnee()-evt.getDate().getAnnee()))-1);
					hashMapEvenementIntitule.put(evt.getAdrImage(), evt);
			}
		}
		}
		
	}
	
	/**
	 * <font color="red">Renvoie une Chronologie</font>
	 * @return Chronologie : renvoie une Chronologie
	 */
	
	public Chronologie getChronologie() {
		return chronologie;
	}
	
	/**
	 * <font color="red">Renvoie une Hashmap contenant les couples cl�s-valeurs ayant pour cl� l'adresse de l'image d'un �v�nement et en valeur l'�v�nement lui-m�me</font>
	 * @return HashMap<String, Evenement> : renvoie une Hashmap d'adressesImages associ�es � leur �v�nement
	 */
	
	public HashMap<String, Evenement> getHashMapEvenementIntitule(){
		return hashMapEvenementIntitule;
	}
	
	/**
	 * <font color="red"> Renvoie tout le temps faux pour empecher l'utilisateur d'agir sur les cellules de la JTable en cliquant dessus dans la fen�tre</font>
	 * @param Int rowIndex correspond � un num�ro de ligne
	 * @param Int columIndex correspond � num�ro de colonne
	 * @return Boolean renvoie un bool�en 
	 */
	
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	/**
	 *  <font color="red">Renvoie la classe de la colonne rentr�e en param�tre. Ici cela renvoie toujours String puisque les colonnes ont toutes pour classe String</font>
	 * @param Int parNum correspond au num�ro de colonne souhait�
	 * @return Class renvoie le type de la classe 
	 */
	
	public Class getColumnClass(int parNum){
		return String.class;
	}
	
}
