package Modele;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.TreeSet;

public class Chronologie implements Serializable {
	
	private Date dateDebut;
	private Date dateFin;
	private int periode;
	private String intitule;
	private String adresseFichier;
	private TreeMap<Integer, TreeSet<Evenement>> tabEvt;
	
	
	
	public Chronologie(Date parDateDebut, Date parDateFin, int parPeriode, String parIntitule, String parAdresseFichier){
		dateDebut=parDateDebut;
		dateFin=parDateFin;
		periode=parPeriode;
		intitule=parIntitule;
		adresseFichier=parAdresseFichier;
		tabEvt=new TreeMap<Integer, TreeSet<Evenement>>();
	}
	
	public Chronologie() {
		dateDebut=new Date();
		dateFin=new Date();
		periode=1;
		intitule="";
		adresseFichier="";
		tabEvt=new TreeMap<Integer, TreeSet<Evenement>>();
		this.ajout(new Evenement());
	}
	
	/**
	 * <font color="red">Permet d'ajouter un �v�nement � la chronologie apr�s v�rification de sa validit�.Les �venements �tant class�s par ann�e
	 * , si l'ann�e de l'�v�nement pass� en param�tre n'existe pas dans les clefs de la TreeMap , alors on cr�� un nouveau couple clefs-valeur ayant pour clefs
	 * l'ann�e de l'�v�nement pass� en param�tre et pour valeur un Treeset contenant l'�v�nement
	 * Si l'ann�e de l'�venement pass� en param�tre est une cl� de la Treemap , on ajoute l'�venement au TreeSet correspondant � cette cl�. </font>
	 * @param parEvenement �venement qui sera rajout� � la chronologie
	 */

	public void ajout(Evenement parEvenement) {
		int numAnnee = parEvenement.getDate().getAnnee();
		if(tabEvt.containsKey(numAnnee)){
			tabEvt.get(numAnnee).add(parEvenement);
		}
		else{
			tabEvt.put(numAnnee, new TreeSet<Evenement>());
			tabEvt.get(numAnnee).add(parEvenement);
		}
		
		if(tabEvt.containsKey(parEvenement.getDate().getAnnee())){
			tabEvt.get(parEvenement.getDate().getAnnee()).add(parEvenement);
		}
		else{
			tabEvt.put(parEvenement.getDate().getAnnee(), new TreeSet<Evenement>());
			tabEvt.get(parEvenement.getDate().getAnnee()).add(parEvenement);
			
		}
		
	
	}
	
	/**
	 * <font color="red">Permet de supprimer un �venement ,choisi en param�tre, du TreeSet dans lequel il est contenu. </font>
	 * @param parEvenement Evenement parametre qui correspond a l'�venement qui sera supprimer
	 */
	
	public void supprimerEvenement(Evenement parEvenement) {
		tabEvt.get(parEvenement.getDate().getAnnee()).remove(parEvenement);
	}
	
	/**
	 * <font color="red"> Permet d'afficher tous les evenements sous forme d'une chaine de charact�re finale en parcourant toutes le cl�s de la Treemap ainsi que les
	 * TreeSet correspondant � ces cl�s. </font>
	 * @return String renvoie une chaine de charactere afin d'afficher les evenements
	 */
	
	public String afficherEvt(){
		String chaineFinale = "";
		for(int annee : tabEvt.keySet()){
			for(Evenement evt : tabEvt.get(annee)){
				chaineFinale+=evt.toString() + "\n";
			}
		}
		
		return chaineFinale;	
		
	}
	
	/**
	 * <font color="red">Permet d'afficher le contenu de la chronologie sous forme de String , soit son intitul� , la date de d�but , la date de fin 
	 * , l'adresse du fichier et tous les �v�nements contenus dans cette frise.</font>
	 * @return String renvoie une chaine de caract�res
	 */
	
	public String toString(){
		
		String chaineEvenement = null;
		for(int annee : tabEvt.keySet()){
			for(Evenement evt : tabEvt.get(annee)){
				chaineEvenement+=evt.toString() + "\n";
			}
		}
		
		
		return intitule + "\n de " + dateDebut + "�" + dateFin + "\n adresse fichier :" + adresseFichier + "\n Evenements :"+chaineEvenement;
	}

	
	/**
	 * <font color="red">Permet de r�cuperer le champ dateDebut d'une chronologie correspondant � la date de d�but d'une chronologie</font>
	 * @return Date renvoie la date de d�but de la chronologie
	 */

	public Date getDateDebut() {
		return dateDebut;
	}


	/**
	 * <font color="red">Permet de r�cuperer le champ dateFin d'une chronologie correspondant � la date de fin d'une chronologie</font>
	 * @return Date renvoie la date de fin de la chronologie
	 */


	public Date getDateFin() {
		return dateFin;
	}

	/**
	 * <font color="red">Permet de r�cuperer le champs p�riode d'une chronologie correspondant � p�riode d'une chronologie</font>
	 * @return Int renvoie la p�riode de la chronologie , c'est � dire le point de rep�re de l'�cart de temps
	 */

	public int getPeriode() {
		return periode;
	}

/**
	 * <font color="red">Permet de r�cuperer le champ intitule d'une chronologie correspondant � l'intitul� d'une chronologie</font>
	 * @return String renvoie l'intitul� de la frise chronologie
	 */

	public String getIntitule() {
		return intitule;
	}
	/**
	 * <font color="red">Permet de r�cuperer le champ adresseFichier d'une chronologie correspondant � l'adresse du fichier dans laquelle la chronologie est �crite. </font>
	 * @return String renvoie l'adresse du fichier dans lequel la frise est �crite 
	 */

	public String getAdresseFichier() {
		return adresseFichier;
	}


	/**
	 * <font color="red">Permet de renvoyer le champ tabEvt  qui est une Treemap de  TreeSet d'�v�nements, qui contient tous les TreeSet dans lesquels sont contenus les �v�nements </font>
	 * @return TreeMap<Integer, TreeSet<Evenement>>
	 */

	public TreeMap<Integer, TreeSet<Evenement>> getTabEvt() {
		return tabEvt;
	}

	/**
	 * <font color="red">Permet de renvoyer le nombre d'�v�nements contenues dans la TreeMap</font>
	 * @return int renvoie la taille du tableau d'�venement
	 */
	
	public int getSizeTabEvt() {
		int resultat=0;
		for(int annee : tabEvt.keySet()){
			for(Evenement evt : tabEvt.get(annee)){
				resultat++;
			}
		}
		return resultat;
	}
	
	
	/**
	 * <font color="red">V�rifie que la chronologie est valide : elle est valide si la date de d�but est inf�rieure � la date de fin</font>
	 * @return renvoie un boolean (True , False) qui d�crit si la date de d�but est inf�rieure � la date de fin
	 */
	public boolean estValide() {
		return dateDebut.compareTo(dateFin)<0;
	}
	
}
